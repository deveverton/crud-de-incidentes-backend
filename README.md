# CRUD de incidentes

Este projeto foi criado com Laravel 8 (https://laravel.com/).

# Requisitos para rodar a aplicação

- PHP ^8.*
- MySQL (Caso opte por salvar os incidentes no banco)
- Git
- Composer

## Instalação

Em alguma pasta de sua preferencia, rode o comando `git clone git@gitlab.com:deveverton/crud-de-incidentes-backend.git`

Na pasta do projeto, execute o comando `composer install`

Após a conclusão, edite as configurações do arquivo .env incluso

Temos 2 tipos de armazenamento de incidentes:

- Database (Necessita de um banco MySQL ou SQL Server)
- Local (Salva todos incidentes na propria pasta da aplicação)

### `Utilizando Database`

Na pasta do projeto, execute o comando `php artisan migrate`, abra o arquivo .env e coloque `APP_STORAGE_TYPE=database ` 

### `Utilizando database local `

Abra o arquivo .env e coloque `APP_STORAGE_TYPE=file. Para ambos, FILESYSTEM_DRIVER deve ser **local-database**

### `Executar a aplicação`

Na pasta raiz, execute o comando `php artisan serve`, a API estará pronta para uso!

