<?php

namespace Database\Seeders;

use App\Models\Incidents;
use Illuminate\Database\Seeder;

class IncidentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Incidents::factory()
            ->count(10)
            ->create();
    }
}
