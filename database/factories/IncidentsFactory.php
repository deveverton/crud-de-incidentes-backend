<?php

namespace Database\Factories;

use App\Models\Incidents;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class IncidentsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Incidents::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'description' => $this->faker->paragraph,
            'criticality' => Arr::random(['high','medium','low']),
            'type' => Arr::random(['alarm','incident','others']),
            'status' => Arr::random(['active','inactive']),
        ];
    }
}
