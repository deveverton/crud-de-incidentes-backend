<?php

namespace App\Http\Controllers;

use App\Http\Requests\IncidentsRequest;
use App\Http\Resources\IncidentsResource;
use App\Services\IncidentsService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class IncidentsController extends Controller
{
    private $service;

    /**
     * IncidentsController constructor.
     * @param IncidentsService $service
     */
    public function __construct(IncidentsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $resources = $this->service->index();
        return IncidentsResource::collection($resources);
    }

    /**
     * Display the specified resource.
     *
     * @param IncidentsRequest $request
     * @return IncidentsResource
     * @throws \App\Exceptions\ApiException
     */
    public function show(IncidentsRequest $request)
    {
        $resource = $this->service->show($request->id);
        return new IncidentsResource($resource);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IncidentsRequest $request
     * @return IncidentsResource
     */
    public function store(IncidentsRequest $request)
    {
        $resource = $this->service->store($request->all());
        return new IncidentsResource($resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param IncidentsRequest $request
     * @param int $id
     * @return IncidentsResource
     */
    public function update(IncidentsRequest $request)
    {
        Log::alert($request->id);
        $data = $this->service->update($request->id, $request->all());
        return new IncidentsResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param IncidentsRequest $request
     * @return IncidentsResource
     * @throws \App\Exceptions\ApiException
     */
    public function destroy(IncidentsRequest $request)
    {
        $resource = $this->service->show($request->id);
        $this->service->destroy($request->id);
        return new IncidentsResource($resource);
    }
}
