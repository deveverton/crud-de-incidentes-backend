<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class IncidentsRequest extends FormRequest
{
    public function prepareForValidation()
    {
        if($this->method() !== 'POST'){
            return $this->merge([
                'id' => env('APP_STORAGE_TYPE') === 'database' ? $this->route('incident') : $this->route('incident').'.json'
            ]);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dataRules = [
            'title'         => 'required|string|min:2|max:255',
            'description'   => 'required|string|min:2',
            'criticality'   => 'required|in:high,medium,low',
            'type'          => 'required|in:alarm,incident,others',
            'status'        => 'required|in:active,inactive',
        ];

        $idRule = [
            'id' => [
                'required', function($attribute, $value, $fail){
                    if(env('APP_STORAGE_TYPE') === 'files'){
                        if(!Storage::exists($value)){
                            return $fail(__('notfound'));
                        }
                        return true;
                    }else{
                        return Rule::exists('incidents', 'id');
                    }
                }]
            ];

        return match($this->getMethod()){
            'POST' => $dataRules,
            'PUT'  => array_merge($dataRules, $idRule),
            'GET', 'DELETE' => $idRule
        };
    }
}
