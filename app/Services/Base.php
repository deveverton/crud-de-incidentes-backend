<?php


namespace App\Services;


use App\Storage\Handler;

class Base
{
    public $storageAdapter;

    /**
     * Base constructor.
     * @param $storageAdapter
     */
    public function __construct(Handler $storageAdapter)
    {
        $this->storageAdapter = $storageAdapter->getAdapter();
    }
}
