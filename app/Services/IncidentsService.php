<?php


namespace App\Services;


use App\Exceptions\ApiException;

class IncidentsService extends Base
{
    public function index()
    {
        $resources = $this->storageAdapter->index();
        return $resources;
    }

    public function show($id)
    {
        $resource = $this->storageAdapter->show($id);

        if(!$resource){
            throw new ApiException(__('not_found'), 404);
        }

        return $resource;
    }

    public function store($data)
    {
        $response = $this->storageAdapter->store($data);
        return $response;
    }

    public function update($id, $data)
    {
        $response = $this->storageAdapter->update($id, $data);
        return $response;
    }

    public function destroy($id)
    {
        $response = $this->storageAdapter->destroy($id);
        return $response;
    }
}
