<?php


namespace App\Storage;


use App\Storage\Adapters\DatabaseStorageAdapter;
use App\Storage\Adapters\FileStorageAdapter;
use App\Storage\Adapters\IStorageAdapter;

class Handler
{
    private $adapter;

    /**
     * Handler constructor.
     */
    public function __construct()
    {
        $this->adapter = env('APP_STORAGE_TYPE') === IStorageAdapter::ADAPTER_DATABASE
            ? new DatabaseStorageAdapter()
            : new FileStorageAdapter();
    }

    /**
     * @return IStorageAdapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }
}
