<?php


namespace App\Storage\Adapters;


use Illuminate\Support\Facades\Storage;

class FileStorageAdapter implements IStorageAdapter
{
    public function index()
    {
        $incidents = Storage::allFiles();
        return array_map(fn($file) => $this->getFileFromStorage($file), $incidents);
    }

    public function show($id)
    {
        return Storage::exists($id) ? $this->getFileFromStorage($id) : [];
    }

    public function store($data)
    {
        $filename = uniqid();
        Storage::put($filename.'.json', json_encode($data));
        return array_merge($data, [
            'id' => $filename
        ]);
    }

    public function update($id, $data)
    {
        unset($data['id']);
        Storage::put($id, json_encode($data));
        return $data;
    }

    public function destroy($id)
    {
        Storage::delete($id);
    }

    private function parseData($file, $id)
    {
        return array_merge(json_decode($file, true), [
            'id' => str_replace(".json", "", $id)
        ]);
    }

    private function getFileFromStorage($file)
    {
        return $this->parseData(Storage::get($file), $file);
    }
}
