<?php


namespace App\Storage\Adapters;


use App\Models\Incidents;

class DatabaseStorageAdapter implements IStorageAdapter
{

    public function index()
    {
        return Incidents::all();
    }

    public function show($id)
    {
        return Incidents::find($id);
    }

    public function store($data)
    {
        return Incidents::create($data);
    }

    public function update($id, $data)
    {
        Incidents::find($id)->fill($data)->save();
        return $data;
    }

    public function destroy($id)
    {
        return Incidents::destroy($id);
    }
}
