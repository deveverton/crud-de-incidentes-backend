<?php


namespace App\Storage\Adapters;

interface IStorageAdapter
{
    const ADAPTER_FILE = 'file';
    const ADAPTER_DATABASE = 'database';

    public function index();
    public function show($id);
    public function store($data);
    public function update($id, $data);
    public function destroy($id);
}
